In questo repository sono raccolti i codici che ho utilizzato per l'esame di AIAP.

In particolare:
* nel codice DataPreparation.ipjyb è stata fatta la data preparation del dataset in esame
* i codici *.py sono quelli relativi all'addestramento dei classificatori e sono stati fatti girare sulla macchina virtuale del CERN
* i codici ana*.ipynb sono relativi ai codici *.py e vi è l'analisi dello scorign dei classificatori