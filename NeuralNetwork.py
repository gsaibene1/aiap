#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 16 14:14:09 2023
@author: famig

Creo questo codice in Spyder per poter farlo girare sulla macchina virtuale
del CERN così da non appesantire troppo il mio PC.
Inoltre, l'ambiente di Spyder mi permette di debuggare più facilmente rispetto
ad un quaderno Jupyter, dove svolgerò l'analisi dei risultati
(anaNeuralNetwork.ipynb)
"""

#%% Import moduli necessari all'analisi
import numpy as np
import time

from sklearn import preprocessing
from sklearn.model_selection import ParameterGrid

# Per la K-Fold validation
from sklearn.model_selection import RepeatedStratifiedKFold
# Per la PCA
from sklearn.decomposition import PCA
# Per lo Scoring
from sklearn import metrics
# Classificatore Neural Network
from sklearn.neural_network import MLPClassifier


#%% Definizioni preliminari

prefisso = "NeuralNetwork"
resultFile = f"Results/{prefisso}.npz"

print(f"Benvenuto nel programma {prefisso}")
print(f"Gli output saranno salvati in {resultFile}")

### Fisso la soglia della Explained Variance per il pruning delle componenti
thrEV = .95

### Fisso i parametri per la Grid search
# Numero di neuroni in ogni layer
hidden_layer_sizes = [np.array(( 64, )),
                       np.array(( 64, 32 )),
                       np.array(( 64, 32, 16 )),
                       np.array(( 128, 64, 32 )),
                       np.array(( 128, 64, 32, 16 )),
                       np.array(( 128, 128, 64, 64 ))]
# Funzione di attivazione dell'hidden layer
activation = ["identity", "logistic", "tanh", "relu"]       
# Learning rate iniziale per aggiornare i pesi
learning_rate_init = [0.001, 0.01, 0.1]         
# Learning rate per aggiornare i pesi
learning_rate = ["constant", "invscaling"]


### Definisco un dizionario per la Grid search
paramDict = {"hidden_layer_sizes": hidden_layer_sizes, "activation": activation,
             "learning_rate_init": learning_rate_init, "learning_rate": learning_rate}


### Fisso i parametri per la ROC Curve
# Numero punti per curva ROC
nrocstep = 1000
# Vettore su cui interpolare curva ROC
xROC = np.linspace(0, 1, nrocstep)


#%% Load dei dati
dati = np.load("dataSet/dati_finali.npz")

X = dati["X"]
y = dati["y"]
# features = dati["features"]


#%% Grid search

# Definisco la grid search
list_grid = list(ParameterGrid(paramDict))
nStepGrid = len(list_grid)
print(f"Saranno provate {len(list_grid)} configurazioni di iper-parametri")


#%% Definisco quante K-Fold fare
"""
Faccio 5 folders in modo tale da usare il 20% come test set (n_splits = 5)
Faccio 2 ripetizioni in modo tale da fare dieci validazioni per ciascun 
dataset e per ciascuna configurazione di iper-parametri (n_repeats = 2)
"""
rskf = RepeatedStratifiedKFold(n_splits = 5, n_repeats = 2, random_state = 13)

nStepKF = rskf.get_n_splits(X, y)
print(f"Saranno effettuati {nStepKF} step di K-Fold per ciascuna configurazione di iper-parametri")


#%% Vettori globali di scoring
"""
Per ogni valore di scoring, creo un vettore di zeri della giusta grandezza
dove andrò a salvare il valore medio delle K-Fold e il suo relativo errore (std)
così da analizzare i risultati successivamente.
"""

### Valori medi
globConfusionMatrix = np.zeros((2, 2, nStepGrid), dtype = float)

globAccuracy    = np.zeros(nStepGrid, dtype = float)
globPrecision   = np.zeros(nStepGrid, dtype = float)
globRecall      = np.zeros(nStepGrid, dtype = float)
globF1          = np.zeros(nStepGrid, dtype = float)

globxROC        = np.zeros((nrocstep, nStepGrid), dtype = float)
globyROC        = np.zeros((nrocstep, nStepGrid), dtype = float)
globAUC         = np.zeros(nStepGrid, dtype = float)


### Errori
globConfusionMatrixErr = np.zeros((2, 2, nStepGrid), dtype = float)

globAccuracyErr     = np.zeros(nStepGrid, dtype = float)
globPrecisionErr    = np.zeros(nStepGrid, dtype = float)
globRecallErr       = np.zeros(nStepGrid, dtype = float)
globF1Err           = np.zeros(nStepGrid, dtype = float)

globxROCErr         = np.zeros((nrocstep, nStepGrid), dtype = float)
globyROCErr         = np.zeros((nrocstep, nStepGrid), dtype = float)
globAUCErr          = np.zeros(nStepGrid, dtype = float)


### Iper-parametri, esporto anche loro come liste
iperParam_hidden_layer_sizes        = []
iperParam_activation                = []
iperParam_learning_rate_init        = []
iperParam_learning_rate             = []



#%% Analisi MLPClassifier

# Salvo il tempo iniziale così valuto quanto tempo ci mette in totale
T = time.time()


### Ciclo sulle combinazioni di iper parametri
for j, grid_dict in enumerate(list_grid):
    
    ### Estraggo i parametri dello step corrente grid search
    tmp_hidden_layer_sizes = grid_dict["hidden_layer_sizes"]
    tmp_activation = grid_dict["activation"]
    tmp_learning_rate_init = grid_dict["learning_rate_init"]
    tmp_learning_rate = grid_dict["learning_rate"]
    
    ### Li appendo a delle liste per poterli esportare
    iperParam_hidden_layer_sizes.append(tmp_hidden_layer_sizes)
    iperParam_activation.append(tmp_activation)
    iperParam_learning_rate_init.append(tmp_learning_rate_init)
    iperParam_learning_rate.append(tmp_learning_rate)
    
    
    ### Resetto i parametri per lo scoring che medierò sulle K-Fold
    tmpConfusionMatrix = np.zeros((2, 2, nStepKF), dtype = float)
    
    tmpAccuracy     = np.zeros(nStepKF, dtype = float)
    tmpPrecision    = np.zeros(nStepKF, dtype = float)
    tmpRecall       = np.zeros(nStepKF, dtype = float)
    tmpF1           = np.zeros(nStepKF, dtype = float)
    
    # ROC Curves
    # Punti densi per calcolo aree
    tmpxROC = np.zeros((nrocstep, nStepKF), dtype = float)
    tmpyROC = np.zeros((nrocstep, nStepKF), dtype = float)
    tmpAUC = np.zeros(nStepKF, dtype = float)
    
    
    # K-Fold: Itero su una certa combinazione di TrS/TeS
    for i, (train_index, test_index) in enumerate(rskf.split(X, y)):
        
        print(f"\n\n\nStep grid Search: {j+1} / {nStepGrid}\tStep K-Fold: {i+1} / {nStepKF}")
        
        # Ottengo TrS e TeS usando le varie K-Fold
        X_train = X[train_index,:]
        y_train = y[train_index]
        X_test = X[test_index,:]
        y_test = y[test_index]
        
        
        ### Data Preparation
        # Applico scaling MinMax (come visto nel codice precedente è il migliore)
        scaler = preprocessing.MinMaxScaler()
        scaler.fit(X_train)
        X_train_scaled = scaler.transform(X_train)
        
        # Applico la PCA
        pca = PCA()
        pca.fit(X_train_scaled)
        X_train_pca = pca.transform(X_train_scaled)
        
        # Estraggo le Explained Variances
        EV = pca.explained_variance_ratio_
        cumEV = np.cumsum(EV)
        
        
        # Stabilisco che componenti tenere per il pruning
        idxLastFeature = np.sum(cumEV <= thrEV)
        print(f"Per avere il {thrEV * 100} % di Explaied Variance, sono necessarie {idxLastFeature} features ")
        
        # Faccio il pruning tenendo solo le prime componenti più informative
        X_train_pruned = X_train_pca[:,:idxLastFeature]
        
        # Riscalo, applico la PCA e pruno il TsS
        X_test_scaled = scaler.transform(X_test)
        X_test_pca = pca.transform(X_test_scaled)
        X_test_pruned = X_test_pca[:,:idxLastFeature]
        
        
        
        ### Definisco i parametri del MLPClassifier
        classifier = MLPClassifier(hidden_layer_sizes = tmp_hidden_layer_sizes,
                                   activation = tmp_activation,
                                   learning_rate_init = tmp_learning_rate_init,
                                   learning_rate = tmp_learning_rate,
                                   early_stopping = True, n_iter_no_change = 15,
                                   tol = 1e-4)
        
        
        ### Training
        t = time.time()
        classifier.fit(X_train_pruned, y_train)
        print(f"Tempo passato per il training: {time.time()-t:.2f} s")
        
        
        ### Prediction
        t = time.time()
        y_pred = classifier.predict(X_test_pruned)
        y_pred_proba = classifier.predict_proba(X_test_pruned)[:,1]
        print(f"Tempo passato per la prediction: {time.time()-t:.2f} s")
        
        
        ### Valuto come si è comportato il classificatore
        confusionMatrix =   metrics.confusion_matrix(y_test, y_pred)
        accuracy        =   metrics.accuracy_score(y_test, y_pred)
        precision       =   metrics.precision_score(y_test, y_pred)
        recall          =   metrics.recall_score(y_test, y_pred)
        f1              =   metrics.f1_score(y_test, y_pred)
        
        # Salvo gli scoring parziali
        tmpConfusionMatrix[:,:,i] = confusionMatrix
        tmpAccuracy[i] = accuracy
        tmpPrecision[i] = precision
        tmpRecall[i] = recall
        tmpF1[i] = f1
        
        
        ### Valuto la ROC Curve
        fpr, tpr, thresholds = metrics.roc_curve(y_test, y_pred_proba)
        
        # Creo un'interpolazione densa per miglior calcolo aree
        yROC = np.interp(xROC, fpr, tpr)
        myAUC = metrics.auc(xROC, yROC)
        
        # Salvo i valori
        tmpxROC[:,i] = xROC
        tmpyROC[:,i] = yROC
        tmpAUC[i] = myAUC
        
        
        ### Stampo alcuni risultati per verificare in tempo reale come procede
        print("\nResults:")
        print("Confusion Matrix: ")
        print(confusionMatrix)
        print(f"Print: Accuracy: {accuracy:.2f}")
        print(f"Print: precision: {precision:.2f}")
        print(f"Print: recall: {recall:.2f}")
        print(f"Print: f1: {f1:.2f}")
        print(f"Print: AUC: {myAUC:.2f}")
    
    
    
    ### Medio sulle K-Fold che ho fatto
    # Medio le grandezze di scoring sulle varie K-Fold    
    meanConfusionMatrix = np.mean(tmpConfusionMatrix, axis = 2)
    meanAccuracy = np.mean(tmpAccuracy)
    meanPrecision = np.mean(tmpPrecision)
    meanRecall = np.mean(tmpRecall)
    meanF1 = np.mean(tmpF1)
    # Medio le curve ROC
    meanxROC = np.mean(tmpxROC, axis = 1)
    meanyROC = np.mean(tmpyROC, axis = 1)
    meanAUC = np.mean(tmpAUC)
    
    
    ### Valuto gli errori sulle K-Fold (faccio la std)
    # Scoring
    stdConfusionMatrix = np.std(tmpConfusionMatrix, axis = 2) / np.sqrt(nStepKF - 1)
    stdAccuracy = np.std(tmpAccuracy) / np.sqrt(nStepKF - 1)
    stdPrecision = np.std(tmpPrecision) / np.sqrt(nStepKF - 1)
    stdRecall = np.std(tmpRecall) / np.sqrt(nStepKF - 1)
    stdF1 = np.std(tmpF1) / np.sqrt(nStepKF - 1)
    # ROC
    stdxROC = np.std(tmpxROC, axis = 1) / np.sqrt(nStepKF - 1)
    stdyROC = np.std(tmpyROC, axis = 1) / np.sqrt(nStepKF - 1)
    stdAUC = np.std(tmpAUC) / np.sqrt(nStepKF - 1)
    
    
    
    ### Metto le medie e gli errori nei vettori costruiti in precedenza
    globConfusionMatrix[:,:,j] = meanConfusionMatrix
    
    globAccuracy[j] = meanAccuracy
    globPrecision[j] = meanPrecision
    globRecall[j] = meanRecall
    globF1[j] = meanF1
    
    globxROC[:,j] = meanxROC
    globyROC[:,j] = meanyROC
    globAUC[j] = meanAUC
    
    globConfusionMatrixErr[:,:,j] = stdConfusionMatrix
    
    globAccuracyErr[j] = stdAccuracy
    globPrecisionErr[j] = stdPrecision
    globRecallErr[j] = stdRecall
    globF1Err[j] = stdF1
    
    globxROCErr[:,j] = stdxROC
    globyROCErr[:,j] = stdyROC
    globAUCErr[j] = stdAUC


print(f"\n\nFine dell'analisi. Ci ha messo {time.time()-T:.2f} s")


#%% Salvo i dati per analizzare i risultati

np.savez(resultFile, globConfusionMatrix = globConfusionMatrix,
                     globAccuracy = globAccuracy,
                     globPrecision = globPrecision,
                     globRecall = globRecall,
                     globF1 = globF1,
                     
                     globxROC = globxROC,
                     globyROC = globyROC,
                     globAUC = globAUC,
                     
                     globConfusionMatrixErr = globConfusionMatrixErr,
                     globAccuracyErr = globAccuracyErr,
                     globPrecisionErr = globPrecisionErr,
                     globRecallErr = globRecallErr,
                     globF1Err = globF1Err,
                     
                     globxROCErr = globxROCErr,
                     globyROCErr = globyROCErr,
                     globAUCErr = globAUCErr,
                     
                     iperParam_hidden_layer_sizes = np.array(iperParam_hidden_layer_sizes, dtype = object),
                     iperParam_activation = iperParam_activation, 
                     iperParam_learning_rate_init = iperParam_learning_rate_init,
                     iperParam_learning_rate = iperParam_learning_rate)

print(f"Ho salvato i dati in: {resultFile}")

